use a_tree::Tree;

#[derive(Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Default)]
struct Coordinates {
    x: usize,
    y: usize,
}

const ALL_DIRECTIONS: [Direction; 4] = [
    Direction::Up,
    Direction::Down,
    Direction::Left,
    Direction::Right,
];

fn next_level(value: &Coordinates) -> Vec<(Direction, Coordinates)> {
    ALL_DIRECTIONS
        .into_iter()
        .filter_map(|direction| {
            let new_coords = match direction {
                Direction::Up => Coordinates {
                    x: value.x,
                    y: value.y + 1,
                },
                Direction::Right => Coordinates {
                    x: value.x + 1,
                    y: value.y,
                },
                Direction::Down if value.y > 0 => Coordinates {
                    x: value.x,
                    y: value.y - 1,
                },
                Direction::Left if value.x > 0 => Coordinates {
                    x: value.x - 1,
                    y: value.y,
                },
                _ => return None,
            };

            Some((direction, new_coords))
        })
        .collect()
}

fn main() {
    println!("Generating");
    let mut tree = Tree::generations(Coordinates::default(), 3, next_level);

    println!("{:#?}", tree);
    println!("{:?}", tree.leaves());

    println!("Pruning");
    tree.retain(|item| item.x > 0 && item.y > 0);

    println!("{:#?}", tree);
    println!("{:?}", tree.leaves());

    println!("Selecting");
    let mut tree = tree.select(&Direction::Up).unwrap();

    println!("{:#?}", tree);
    println!("{:?}", tree.leaves());

    println!("Next generation");
    tree.next_generation(next_level);

    println!("{:#?}", tree);
    println!("{:?}", tree.leaves());
}
