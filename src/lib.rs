use std::collections::HashMap;
use std::hash::Hash;

#[derive(Debug)]
pub struct Tree<K, T> {
    value: T,
    children: HashMap<K, Tree<K, T>>,
}

impl<K, T> Tree<K, T>
where
    K: Eq + Hash,
{
    pub fn new(item: T) -> Self {
        Tree {
            value: item,
            children: HashMap::new(),
        }
    }

    pub fn generations<F>(start: T, depth: usize, callback: F) -> Self
    where
        F: Fn(&T) -> Vec<(K, T)> + Copy,
    {
        let mut children = HashMap::new();

        if depth != 0 {
            for (key, value) in (callback)(&start) {
                let child = Self::generations(value, depth - 1, callback);
                children.insert(key, child);
            }
        }

        Tree {
            value: start,
            children,
        }
    }

    pub fn next_generation<F>(&mut self, callback: F)
    where
        F: Fn(&T) -> Vec<(K, T)> + Copy,
    {
        if self.children.is_empty() {
            let children = (callback)(&self.value);
            self.children.extend(children.into_iter().map(|(k, v)| {
                (
                    k,
                    Tree {
                        value: v,
                        children: HashMap::new(),
                    },
                )
            }));
        } else {
            for child in self.children.values_mut() {
                child.next_generation(callback);
            }
        }
    }

    pub fn select(mut self, path: &K) -> Option<Self> {
        self.children.remove(path)
    }

    pub fn leaves(&self) -> Vec<(Vec<&K>, &T)> {
        self.do_leaves(vec![])
    }

    fn do_leaves<'a>(&'a self, path: Vec<&'a K>) -> Vec<(Vec<&'a K>, &'a T)> {
        if self.children.is_empty() {
            vec![(path, &self.value)]
        } else {
            self.children
                .iter()
                .flat_map(|(key, child)| {
                    let mut path = path.clone();
                    path.push(key);
                    child.do_leaves(path)
                })
                .collect()
        }
    }

    pub fn retain<C>(&mut self, callback: C)
    where
        C: Fn(&T) -> bool + Copy,
    {
        self.children.retain(|_key, child| {
            child.retain(callback);

            if child.children.is_empty() {
                (callback)(&child.value)
            } else {
                true
            }
        });
    }
}
